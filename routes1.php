<?php
    include($_SERVER['DOCUMENT_ROOT']."/xpto-company/header.php");
?>

<main class="products">
    <section class="cart">
        <div class="row p-4">
            <div class="col-md-8">
                <div class = "wrapper routes-tbl-wrapper">
                    <table class="table routes-tbl">
                        <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Warehouse</th>
                                <th scope="col">Route Name</th>
                                <th scope="col">Cost</th>
                                <th scope="col">Shipping Time</th>
                                <th scope="col">Status</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $conn = mysqli_connect("localhost", "root", "", "xpto");
                                if ($conn -> connect_error){
                                    die("connection failed:".$conn-> connect_error);
                                }
                                $sql = "SELECT * FROM routes;";
                                $result= $conn->query($sql);
                                if ($result-> num_rows > 0)
                                {
                                    while($row = $result-> fetch_assoc())
                                    {
                                        echo "<tr><td>". $row["route_id"]."</td><td>". $row["origin_route"]."</td><td>". $row["route_name"]."</td><td>". $row["route_cost"]."</td><td>". $row["delivery_time"]."</td><td>". $row["route_status"]."</td><td>
                                        <button href='includes/routes-update.php?id=".$row["route_id"]."'>Edit</button>
                                        <a href='includes/routes-delete.php?id=".$row["route_id"]."'><button>Delete</button></a>
                                        </td>
                                        </tr>";
                                    }
                                        echo "</tbody></table>";
                                }   else {
                                        echo "0 results";
                                }
                            ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
            <form action="includes/routes-update.php" method="POST" class="update-routes-form">
            <input type="hidden" id="route_id value=<?php echo $id;?>">

            <?php
                                $conn = mysqli_connect("localhost", "root", "", "xpto");
                                if ($conn -> connect_error){
                                    die("connection failed:".$conn-> connect_error);
                                }
                                $sql = "SELECT * FROM routes";
                                $result= $conn->query($sql);
                                if ($result-> num_rows > 0)
                                {
                                  $fetch = $result->fetch_assoc();
                                  extract($fetch);
                                ?>
                                  <label for="recipient-name" class="col-form-label">Warehouse:</label>
                                  <input type="text" name="warehouse" class="form-control" id="warehouse-name" value="<?php echo $warehouse; ?>">
                                  <label for="message-text" class="col-form-label">Route Name:</label>
                                  <input type="text" name="routename" class="form-control" id="route-name" value="<?php echo $route_name; ?>">
                                  <label for="message-text" class="col-form-label">Route Cost:</label>
                                  <input type="text" name="routecost" class="form-control" id="route-cost" value="<?php echo $route_cost; ?>">
                                  <label for="message-text" class="col-form-label">Shipping Time:</label>
                                  <input type="datetime-local" name="shippingtime" class="form-control" id="shipping-time" value="<?php echo $delivery_time; ?>">
                                  <label for="message-text" class="col-form-label">Status:</label>
                                  <input type="text" name="status" class="form-control" id="route-status" value="<?php echo $route_status; ?>">
                                  <button class="btn" type="submit" name="save" >Save</button> 

                                <?php
                                }   else {
                                        echo "0 results";
                                }
                            ?>
            </form>
            </div>
        </div>
    </section>
</main>