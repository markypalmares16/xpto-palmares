<?php
    include($_SERVER['DOCUMENT_ROOT']."/xpto-company/header.php");
?>

<main class="products">
    <section class="cart">
        <div class="row p-4">
            <div class="col-md-8">
            <div class="row">
                    <div class="col-md-12 d-flex justify-content-end row-add-btn">
                        <button class="btn" id="route-btnadd">Assign User</button>
                    </div>
                </div>   
                <div class="row">
                </div>
                <div class="wrapper routes-tbl-wrapper">
                    <table class="table routes-tbl">
                        <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Address</th>
                                <th scope="col">Email</th>
                                <th scope="col">Status</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $conn = mysqli_connect("localhost", "root", "", "xpto");
                                if ($conn -> connect_error){
                                    die("connection failed:".$conn-> connect_error);
                                }
                                $sql = "SELECT * FROM account;";
                                $result= $conn->query($sql);
                                if ($result-> num_rows > 0){
                                    while($row = $result-> fetch_assoc()){
                                        echo "<tr><td>". $row["acc_name"]."</td><td>". $row["acc_address"]."</td><td>". $row["acc_email"]."</td><td>". $row["acc_type"]."</td><td>
                                        <button class='btn'>Assign</button>
                                        <button class='btn'>Delete</button></td></tr>";
                                    }
                                    echo "</tbody></table>";
                                } else {
                                    echo "0 results";
                                }
                            ?>
                </div>
            </div>


            <div class="col-md-4" id="routes-update">
                <?php
                    if(isset($_GET['id']) && $_GET['id']!=''){
                    $conn = mysqli_connect("localhost", "root", "", "xpto");
                    if ($conn -> connect_error){
                    die("connection failed:".$conn-> connect_error);
                    }
                    $sql =  "SELECT * FROM routes WHERE route_id=".$_GET['id']."";
                    $result = $conn->query($sql);
                    $rows = $result->fetch_assoc();
                ?>
                <form action="includes/routes-update.php" method="POST" class="route-forms">
                    <h3>Update Route</h3>
                    <input type="hidden" name="route_id" id="route_id" value="<?php echo $rows['route_id'];?>">
                    <label for="recipient-name" class="col-form-label">Warehouse:</label>
                    <input type="text" name="warehouse" class="form-control" id="warehouse-name"
                        value="<?php echo $rows['origin_route']; ?>">
                    <label for="message-text" class="col-form-label">Route Name:</label>
                    <input type="text" name="routename" class="form-control" id="route-name"
                        value="<?php echo $rows['route_name']; ?>">
                    <label for="message-text" class="col-form-label">Route Cost:</label>
                    <input type="text" name="routecost" class="form-control" id="route-cost"
                        value="<?php echo $rows['route_cost']; ?>">
                    <label for="message-text" class="col-form-label">Shipping Time:</label>
                    <input type="datetime-local" name="shippingtime" class="form-control" id="shipping-time"
                        value="<?php echo date_format(strtotime($rows['delivery_time']),'dd/mm/yyyy h:i a') ?>">
                    <label for="message-text" class="col-form-label">Status:</label>
                    <input type="text" name="status" class="form-control" id="route-status"
                        value="<?php echo $rows['route_status']; ?>">
                    <button class="btn mt-3" id="routeupdate-btncancel" type="button" name="cancel">Cancel</button>
                    <button class="btn mt-3" type="submit" name="save">Save</button>
                </form>
                <?php
                    }
                ?>
            </div>
        </div>
    </section>
    <script>
        $(document).ready(function () {
            $("#routeupdate-btncancel").click(function () {
                $("#routes-update").hide();
            });
        });
    </script>
    <script>
        $("#routes-add").hide();
        $(document).ready(function () {
            $("#route-btnadd").click(function () {
                $("#routes-add").show();
            });
        });
        $(document).ready(function () {
            $("#routeadd-btncancel").click(function () {
                $("#routes-add").hide();
            });
        });
    </script>
</main>