<?php
    include($_SERVER['DOCUMENT_ROOT']."/xpto-company/header.php");
?>

<main class="products">
    <section class="products-sec1">
        <div class="row p-4">
            <div class="col-md-8">   
                <div class = "wrapper routes-tbl-wrapper">
                    <table class="table routes-tbl">
                    <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Product Brand</th>
                            <th scope="col">Product Model</th>
                            <th scope="col">Product Cost</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        $conn = mysqli_connect("localhost", "root", "", "xpto");
                        if ($conn -> connect_error){
                        die("connection failed:".$conn-> connect_error);
                        }
                        $sql = "SELECT * FROM products;";
                        $result= $conn->query($sql);
                        if ($result-> num_rows > 0)
                        {
                        while($row = $result-> fetch_assoc())
                        {
                        echo "<tr><td class='route-tbl-col'>". $row["prod_id"]."</td><td class='route-tbl-col'>". $row["prod_brand"]."</td><td class='route-tbl-col'>". $row["prod_model"]."</td><td class='route-tbl-col'>". $row["prod_cost"]."</td></tr>";
                        }
                        echo "</tbody></table>";
                        } else {
                        echo "0 results";
                        }
                    ?>
                </div>
                <div class="row">
                    <div class="col-md-12 d-flex justify-content-end row-add-btn">
                        <a href="products.php"><button class="btn" id="route-btnadd">See all Products</button></a>
                    </div>
                </div> 
            </div>
        </div>
    </section>

    <section class="route-sec1">
        <div class="row p-4">
            <div class="col-md-8">    
                <div class = "wrapper routes-tbl-wrapper">
                    <table class="table routes-tbl">
                    <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Warehouse</th>
                        <th scope="col">Route Name</th>
                        <th scope="col">Cost</th>
                        <th scope="col">Shipping Time</th>
                        <th scope="col">Distance</th>
                        <th scope="col">Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                        $conn = mysqli_connect("localhost", "root", "", "xpto");
                        if ($conn -> connect_error){
                        die("connection failed:".$conn-> connect_error);
                        }
                        $sql = "SELECT * FROM routes;";
                        $result= $conn->query($sql);
                        if ($result-> num_rows > 0)
                        {
                        while($row = $result-> fetch_assoc())
                        {
                        echo "<tr><td class='route-tbl-col'>". $row["route_id"]."</td><td class='route-tbl-col'>". $row["origin_route"]."</td><td class='route-tbl-col'>". $row["route_name"]."</td><td class='route-tbl-col'>". $row["route_cost"]."</td><td class='route-tbl-col'>". $row["delivery_time"]."</td><td class='route-tbl-col'>".$row["route_distance"]."</td><td class='route-tbl-col'>". $row["route_status"]."</td></tr>";
                        }
                        echo "</tbody></table>";
                        }   else {
                        echo "0 results";
                        }
                    ?>
                </div>
                <div class="row">
                    <div class="col-md-12 d-flex justify-content-end row-add-btn">
                        <a href="routes.php"><button class="btn" id="route-btnadd">See all Routes</button></a>
                    </div>
                </div>
            </div> 
        </div>
    </section>
</main>