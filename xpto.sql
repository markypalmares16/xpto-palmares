-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 17, 2020 at 10:42 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `xpto`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `acc_id` int(60) NOT NULL,
  `acc_name` varchar(100) NOT NULL,
  `acc_address` varchar(100) NOT NULL,
  `acc_type` varchar(100) NOT NULL,
  `acc_email` varchar(100) NOT NULL,
  `acc_password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`acc_id`, `acc_name`, `acc_address`, `acc_type`, `acc_email`, `acc_password`) VALUES
(9, 'Mark Gabriel Palmares', 'Iloilo City', 'user', 'palmares@sample.com', '12345'),
(10, 'admin', 'Bacolod City', 'admin', 'admin@admin.com', '12345');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `cart_id` int(100) NOT NULL,
  `acc_id` int(100) NOT NULL,
  `prod_id` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `deliveries`
--

CREATE TABLE `deliveries` (
  `deli_id` int(100) NOT NULL,
  `acc_id` int(100) NOT NULL,
  `prod_id` int(100) NOT NULL,
  `route_id` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `destinations`
--

CREATE TABLE `destinations` (
  `desti_id` int(100) NOT NULL,
  `desti_address` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `destinations`
--

INSERT INTO `destinations` (`desti_id`, `desti_address`) VALUES
(7, 'Bacolod City'),
(15, 'Borongan City'),
(17, 'Catarman'),
(16, 'Catbalogan City'),
(8, 'Dumaguete City'),
(2, 'Iloilo City'),
(6, 'Jordan'),
(3, 'Kalibo'),
(13, 'Maasin City'),
(9, 'Metro Cebu'),
(14, 'Naval'),
(5, 'Roxas City'),
(4, 'San Jose de Buenavista'),
(10, 'Siquijor'),
(12, 'Tacloban City'),
(11, 'Tagbilaran City');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `prod_id` int(100) NOT NULL,
  `prod_brand` varchar(100) NOT NULL,
  `prod_model` varchar(100) NOT NULL,
  `prod_cost` int(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`prod_id`, `prod_brand`, `prod_model`, `prod_cost`) VALUES
(2, 'Gucci', '123stereo', 90000),
(3, 'dior', 'd', 222);

-- --------------------------------------------------------

--
-- Table structure for table `routes`
--

CREATE TABLE `routes` (
  `route_id` int(100) NOT NULL,
  `route_name` varchar(100) NOT NULL,
  `delivery_time` datetime(6) NOT NULL,
  `route_cost` int(100) NOT NULL,
  `route_status` varchar(100) NOT NULL,
  `origin_route` varchar(60) NOT NULL,
  `route_distance` int(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `routes`
--

INSERT INTO `routes` (`route_id`, `route_name`, `delivery_time`, `route_cost`, `route_status`, `origin_route`, `route_distance`) VALUES
(7, 'Victorias City, Negros Occidental, Ajuy, Iloilo, Roxas City Capiz', '0000-00-00 00:00:00.000000', 1212, 'Available', 'sds', 13),
(10, 'M. Cebu to Tagbiliaran City', '2020-04-15 15:03:00.000000', 33, 'Available', 'Metro Cebu', 0),
(11, 'wmwm', '2020-04-15 15:00:00.000000', 2121, 'Not available', 'Metro Cebu', 0),
(12, 'M. Davao to Cagayan De Oro', '2020-04-15 14:03:00.000000', 900, 'Available', 'Metro Davao', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`acc_id`),
  ADD KEY `acc_address` (`acc_address`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`cart_id`),
  ADD KEY `acc_id` (`acc_id`),
  ADD KEY `prod_id` (`prod_id`);

--
-- Indexes for table `deliveries`
--
ALTER TABLE `deliveries`
  ADD PRIMARY KEY (`deli_id`),
  ADD KEY `acc_id` (`acc_id`),
  ADD KEY `prod_id` (`prod_id`),
  ADD KEY `route_id` (`route_id`);

--
-- Indexes for table `destinations`
--
ALTER TABLE `destinations`
  ADD PRIMARY KEY (`desti_id`),
  ADD KEY `desti_address` (`desti_address`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`prod_id`);

--
-- Indexes for table `routes`
--
ALTER TABLE `routes`
  ADD PRIMARY KEY (`route_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account`
--
ALTER TABLE `account`
  MODIFY `acc_id` int(60) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `cart_id` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `deliveries`
--
ALTER TABLE `deliveries`
  MODIFY `deli_id` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `destinations`
--
ALTER TABLE `destinations`
  MODIFY `desti_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `prod_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `routes`
--
ALTER TABLE `routes`
  MODIFY `route_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `account`
--
ALTER TABLE `account`
  ADD CONSTRAINT `account_ibfk_1` FOREIGN KEY (`acc_address`) REFERENCES `destinations` (`desti_address`);

--
-- Constraints for table `cart`
--
ALTER TABLE `cart`
  ADD CONSTRAINT `cart_ibfk_1` FOREIGN KEY (`acc_id`) REFERENCES `account` (`acc_id`),
  ADD CONSTRAINT `cart_ibfk_2` FOREIGN KEY (`prod_id`) REFERENCES `products` (`prod_id`);

--
-- Constraints for table `deliveries`
--
ALTER TABLE `deliveries`
  ADD CONSTRAINT `deliveries_ibfk_1` FOREIGN KEY (`acc_id`) REFERENCES `account` (`acc_id`),
  ADD CONSTRAINT `deliveries_ibfk_2` FOREIGN KEY (`prod_id`) REFERENCES `products` (`prod_id`),
  ADD CONSTRAINT `deliveries_ibfk_3` FOREIGN KEY (`route_id`) REFERENCES `routes` (`route_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
