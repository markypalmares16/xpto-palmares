<?php
    include($_SERVER['DOCUMENT_ROOT']."/xpto-company/header.php");
?>

<main class="products">
    <section class="products-sec1">
        <div class="row p-4">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-12 d-flex justify-content-end row-add-btn">
                        <button class="btn" id="route-btnadd">Add Product</button>
                    </div>
                </div>    
                <div class = "wrapper routes-tbl-wrapper">
                    <table class="table routes-tbl">
                        <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Product Brand</th>
                                <th scope="col">Product Model</th>
                                <th scope="col">Product Cost</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $conn = mysqli_connect("localhost", "root", "", "xpto");
                                if ($conn -> connect_error){
                                    die("connection failed:".$conn-> connect_error);
                                }
                                $sql = "SELECT * FROM products;";
                                $result= $conn->query($sql);
                                if ($result-> num_rows > 0)
                                {
                                    while($row = $result-> fetch_assoc())
                                    {
                                        echo "<tr><td class='route-tbl-col'>". $row["prod_id"]."</td><td class='route-tbl-col'>". $row["prod_brand"]."</td><td class='route-tbl-col'>". $row["prod_model"]."</td><td class='route-tbl-col'>". $row["prod_cost"]."</td><td>
                                        <a href='products.php?id=".$row["prod_id"]."'><button class='btn'>Edit</button></a>
                                        <a href='includes/products-delete.php?id=".$row["prod_id"]."'><button class='btn ml-2'>Delete</button></a>
                                        </td>
                                        </tr>";
                                    }
                                        echo "</tbody></table>";
                                }   else {
                                        echo "0 results";
                                }
                            ?>
                </div>
            </div>

            <div class="col-md-4" id="routes-add">
                <form action="includes/products-add.php" method="POST" class="route-forms">
                    <h3>Add Product</h3>
                    <label for="product-brand" class="col-form-label">Product Brand:</label>
                    <input type="text" name="productbrand" class="form-control" id="product-brand">
                    <label for="product-model" class="col-form-label">Product Model:</label>
                    <input type="text" name="productmodel" class="form-control" id="product-model">
                    <label for="product-cost" class="col-form-label">Product Cost:</label>
                    <input type="text" name="productcost" class="form-control" id="product-cost">
                    <button id="routeadd-btncancel" type="button" name="cancel" name="submit" class="btn mt-3">Cancel</button>
                    <button type="submit" name="submit" class="btn mt-3">Add Product</button>
                </form>
            </div>

            <div class="col-md-4" id="routes-update">
                <?php
                    if(isset($_GET['id']) && $_GET['id']!=''){
                    $conn = mysqli_connect("localhost", "root", "", "xpto");
                    if ($conn -> connect_error){
                    die("connection failed:".$conn-> connect_error);
                    }
                    $sql =  "SELECT * FROM products WHERE prod_id=".$_GET['id']."";
                    $result = $conn->query($sql);
                    $rows = $result->fetch_assoc();
                ?>
                    <form action="includes/products-update.php" method="POST" class="route-forms">
                    <h3>Edit Product</h3>
                    <input type="hidden" name="productid" id="prod-id" value="<?php echo $rows['prod_id'];?>">
                    <label for="product-brand" class="col-form-label">Product Brand:</label>
                    <input type="text" name="productbrand" class="form-control" id="product-brand" value="<?php echo $rows['prod_brand']; ?>">
                    <label for="product-model" class="col-form-label">Product Model:</label>
                    <input type="text" name="productmodel" class="form-control" id="product-model" value="<?php echo $rows['prod_model']; ?>">
                    <label for="product-cost" class="col-form-label">Product Cost:</label>
                    <input type="text" name="productcost" class="form-control" id="product-cost" value="<?php echo $rows['prod_cost']; ?>">
                    <button class="btn mt-3" id="routeupdate-btncancel" type="button" name="cancel">Cancel</button>                   
                    <button class="btn mt-3" type="submit" name="save" >Save</button> 
                </form>
                <?php
                    }
                ?>
            </div>
        </div>
    </section>
    <script>
        $( document ).ready(function() {
            $("#routeupdate-btncancel").click(function(){
                $("#routes-update").hide();
            });
        });
    </script>
    <script>
        $("#routes-add").hide();
        $( document ).ready(function() {
            $("#route-btnadd").click(function(){
                $("#routes-add").show();
            });
        });
        $( document ).ready(function() {
            $("#routeadd-btncancel").click(function(){
                $("#routes-add").hide();
            });
        });
    </script>
</main>