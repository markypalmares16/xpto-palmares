<?php
    include($_SERVER['DOCUMENT_ROOT']."/xpto-company/header-login.php");
?>


<main class="xpto-log-forms" style="background-color: tomato;">


    <section class="lf-sec1">
        <div class="row justify-content-center">
            <div class="col-md-6 lf-text-content">
                <h1>Welcome!</h1>
                <h5>Shop the latest items at lowest price.</h5>
                <p class="pt-2">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
                    et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                    aliquip ex ea commodo consequat. </p>
            </div>
            <div class="col-md-6">
                <div class="lf-btn d-flex">
                    <h5 class="lf-login">Login</h5>
                    <h5 class="lf-signup">Signup</h5>
                </div>

                <form action="includes/access.php" method="POST" class="login-form">
                    <h3>Login</h3>
                    <div class="row pt-5 flex-column">
                        <label for="email" class="login-label">email:</label>
                        <input type="email" placeholder="name@example.com" name="email" id="email" class="lf-form">
                    </div>
                    <div class="row pt-3 flex-column">
                        <label for="password" class="login-label">password:</label>
                        <input type="password" name="password" id="password" class="lf-form">
                    </div>
                    <div class="row pt-2">
                        <a href="#">
                            <h6>forgot password?</h6>
                        </a>
                    </div>
                    <div class="row pt-4">
                    <button class="login-btn" type="submit" name="login" id="login" >Login</button> 
                    </div>
                </form>

                <form action="includes/signup-add.php" method="post" class="signup-form">
                    <h3>Sign-Up</h3>
                    <div class="row pt-5 flex-column">
                        <label for="username" class="login-label">name:</label>
                        <input type="text" placeholder="Juan Marco" name="username" id="username" class="lf-form">
                    </div>
                    <div class="row pt-3 flex-column">
                        <label for="useraddress" class="login-label">address:</label>
                        <select id="useraddress" name="useraddress">
                            <option><b>Select Address</b></option>
                            <?php
                                $conn = mysqli_connect("localhost", "root", "", "xpto");
                                if ($conn -> connect_error){
                                    die("connection failed:".$conn-> connect_error);
                                }
                                $sql = "SELECT * FROM destinations;";
                                $result= $conn->query($sql);
                                if ($result){
                                    while($row = $result-> fetch_assoc()){
                                        echo "<option>". $row["desti_address"]."</option>";
                                    }
                                } else {
                                    echo "0 results";
                                }
                            ?>
                        </select>
                        <!---
                            <textarea class="lf-textarea" id="address" name="address" rows="3" placeholder="lot number, street, subdivision/village, city/municipality, province/state, country"></textarea>
                            -->
                    </div>
                    <div class="row pt-3 flex-column">
                        <label for="useremail" class="login-label">email:</label>
                        <input type="email" placeholder="name@example.com" name="useremail" id="useremail" class="lf-form">
                    </div>
                    <div class="row pt-3 flex-column">
                        <label for="userpassword" class="login-label">password:</label>
                        <input type="password" name="userpassword" id="userpassword" class="lf-form">
                    </div>
                    <div class="row pt-4">
                    <button type="submit" name="signup-submit" class="signup-btn" href="homepage.php">Sign Up</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
</main>



<script>
    $(".lf-login").css("background", "#fff")
    $(".signup-form").hide();
    $(".lf-signup").css({
        "background": "none",
        "color": "white",
        "border": "none"
    });

    $(".lf-signup").click(function () {
        $(".login-form").hide();
        $(".signup-form").show();
        $(".lf-login").css({
            "background": "none",
            "color": "white",
            "border": "none"
        });
        $(".lf-signup").css({
            "background": "#fff",
            "color": "black"
        });
    });

    $(".lf-login").click(function () {
        $(".login-form").show();
        $(".signup-form").hide();
        $(".lf-signup").css({
            "background": "none",
            "color": "white"
        });
        $(".lf-login").css({
            "background": "#fff",
            "color": "black"
        });
    });

    $(".btn").click(function () {
        $(".input").val("");
    });
</script>
