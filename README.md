XPTO Company

INTRODUCTION
    I have developed a page-responsive desktop system rather than using REST API to test my strength and capability as a front-end web developer in spite of advertence to indispensably use REST API. I develop with plain PHP to execute CRUD because in all honesty, I am lacking ingenuity in back-end programming yet. I have set an example where Metro Cebu, Metro Manila, and Metro Davao are the origin routes. Each metropolitan is incharge of the warehouse or origin per depending on which island(Luzon, Visayas, and Mindanao). The destination is in Capiz, and Samar. My plan for my routes is to determine the shortest and inexpensive routes directing to the destination. I also devised several policies that could support the plan: The farther the destination from the origin, the expensive the cost; the nearer the destination, the cheaper the cost. From Metro Cebu(A) to Capiz(B), I mapped out that the main distributor for Panay island(where Capiz is located) is Ajuy, Iloilo. From Metro Cebu(A), it will pass through Negros Occidental, then Ajuy, Iloilo then Capiz.
    
CONTENT    
    This repository contains folders: (1)CSS, (2)img, and (3)includes where includes folder contains execution codes to perform CRUD. This also contains pages and a xpto.sql file. "xpto.sql" file composes of 6 tables, namely: (1)account, (2)cart, (3)deliveries, (4)destinations, (5)products, and (6)routes.

PROCEDURES
1. Head over to login.php
2. For admin, input "admin@admin.com" for email and "12345" for password. On the other hand, for user, "palmares@example.com" for email and "12345" for password. For admin, you are directed to dashboard while user directs you to homepage. 
