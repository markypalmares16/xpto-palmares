<?php
    include($_SERVER['DOCUMENT_ROOT']."/xpto-company/user-header.php");
?>

<main class="ship-hp">
    <section class="cart-tbl">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class = "wrapper routes-tbl-wrapper">
                        <table class="table routes-tbl">
                            <thead>
                                <tr>
                                    <th scope="col">ID</th>
                                    <th scope="col">Product Name</th>
                                    <th scope="col">Product Brand</th>
                                    <th scope="col">Cost</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                                if(isset($_GET['id']) && $_GET['id']!=''){
                                $conn = mysqli_connect("localhost", "root", "", "xpto");
                                if ($conn -> connect_error){
                                die("connection failed:".$conn-> connect_error);
                                }
                                $sql =  "SELECT * FROM products WHERE prod_id=".$_GET['id']."";
                                $result = $conn->query($sql);
                                $rows = $result->fetch_assoc();
                                echo "<tr><td class='route-tbl-col'>". $row["prod_id"]."</td><td class='route-tbl-col'>". $row["prod_brand"]."</td><td class='route-tbl-col'>". $row["prod_model"]."</td><td class='route-tbl-col'>". $row["prod_cost"]."</td><td>
                                        <a href='includes/products-delete.php?id=".$row["prod_id"]."'><button class='btn ml-2'>Delete</button></a>
                                        </td>
                                        </tr>";
                                    }
                                        echo "</tbody></table>";
                            ?>

                    </div>
                </div>
            </div>

            <div class="row">
            <div class="col-md-6" id="routes-update">
                <?php
                    if(isset($_GET['id']) && $_GET['id']!=''){
                    $conn = mysqli_connect("localhost", "root", "", "xpto");
                    if ($conn -> connect_error){
                    die("connection failed:".$conn-> connect_error);
                    }
                    $sql =  "SELECT * FROM routes WHERE route_id=".$_GET['id']."";
                    $result = $conn->query($sql);
                    $rows = $result->fetch_assoc();
                ?>
                <form action="includes/routes-update.php" method="POST" class="route-forms">
                    <h3>Update Route</h3>
                    <input type="hidden" name="route_id" id="route_id" value="<?php echo $rows['route_id'];?>">
                    <label for="recipient-name" class="col-form-label">Warehouse:</label>
                    <input type="text" name="warehouse" class="form-control" id="warehouse-name"
                        value="<?php echo $rows['origin_route']; ?>">
                    <label for="message-text" class="col-form-label">Route Name:</label>
                    <input type="text" name="routename" class="form-control" id="route-name"
                        value="<?php echo $rows['route_name']; ?>">
                    <label for="message-text" class="col-form-label">Route Cost:</label>
                    <input type="text" name="routecost" class="form-control" id="route-cost"
                        value="<?php echo $rows['route_cost']; ?>">
                    <label for="message-text" class="col-form-label">Shipping Time:</label>
                    <input type="datetime-local" name="shippingtime" class="form-control" id="shipping-time"
                        value="<?php echo date_format(strtotime($rows['delivery_time']),'dd/mm/yyyy h:i a') ?>">
                    <label for="message-text" class="col-form-label">Status:</label>
                    <input type="text" name="status" class="form-control" id="route-status"
                        value="<?php echo $rows['route_status']; ?>">
                    <button class="btn mt-3" id="routeupdate-btncancel" type="button" name="cancel">Cancel</button>
                    <button class="btn mt-3" type="submit" name="save">Save</button>
                </form>
                <?php
                    }
                ?>
            </div>
            </div>
            </div>

        </div>
    </section>
</main>