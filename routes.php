<?php
    include($_SERVER['DOCUMENT_ROOT']."/xpto-company/header.php");
?>

<main class="route">

    <section class="route-sec1">
        <div class="row p-4">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-12 d-flex justify-content-end row-add-btn">
                        <button class="btn" id="route-btnadd">Add Route</button>
                    </div>
                </div>    
                <div class = "wrapper routes-tbl-wrapper">
                    <table class="table routes-tbl">
                        <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Warehouse</th>
                                <th scope="col">Route Name</th>
                                <th scope="col">Cost</th>
                                <th scope="col">Shipping Time</th>
                                <th scope="col">Distance</th>
                                <th scope="col">Status</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $conn = mysqli_connect("localhost", "root", "", "xpto");
                                if ($conn -> connect_error){
                                    die("connection failed:".$conn-> connect_error);
                                }
                                $sql = "SELECT * FROM routes;";
                                $result= $conn->query($sql);
                                if ($result-> num_rows > 0)
                                {
                                    while($row = $result-> fetch_assoc())
                                    {
                                        echo "<tr><td class='route-tbl-col'>". $row["route_id"]."</td><td class='route-tbl-col'>". $row["origin_route"]."</td><td class='route-tbl-col'>". $row["route_name"]."</td><td class='route-tbl-col'>". $row["route_cost"]."</td><td class='route-tbl-col'>". $row["delivery_time"]."</td><td class='route-tbl-col'>".$row["route_distance"]."</td><td class='route-tbl-col'>". $row["route_status"]."</td><td class='d-flex justify-content-center'>
                                        <a href='routes.php?id=".$row["route_id"]."'><button class='btn'>Edit</button></a>
                                        <a href='includes/routes-delete.php?id=".$row["route_id"]."'><button class='btn ml-2'>Delete</button></a>
                                        </td>
                                        </tr>";
                                    }
                                        echo "</tbody></table>";
                                }   else {
                                        echo "0 results";
                                }
                            ?>
                </div>
            </div>

            <div class="col-md-4" id="routes-add">
                <form action="includes/routes-add.php" method="POST" class="route-forms">
                    <h3>Add Route</h3>
                    <label for="recipient-name" class="col-form-label">Warehouse:</label>
                    <input type="text" name="warehouse" class="form-control" id="warehouse-name">
                    <label for="message-text" class="col-form-label">Route Name:</label>
                    <input type="text" name="routename" class="form-control" id="route-name">
                    <label for="message-text" class="col-form-label">Route Cost:</label>
                    <input type="text" name="routecost" class="form-control" id="route-cost">
                    <label for="message-text" class="col-form-label">Shipping Time:</label>
                    <input type="datetime-local" name="shippingtime" class="form-control" id="shipping-time">
                    
                    <label for="route-distance" class="col-form-label">Route Distance:</label>
                    <input type="text" name="routedistance" class="form-control" id="route-distance">
                    
                    <label for="message-text" class="col-form-label">Status:</label>
                    <input type="text" name="status" class="form-control" id="route-status">
                    <button id="routeadd-btncancel" type="button" name="cancel" name="submit" class="btn mt-3">Cancel</button>
                    <button type="submit" name="submit" class="btn mt-3">Add Route</button>
                </form>
            </div>

            <div class="col-md-4" id="routes-update">
                <?php
                    if(isset($_GET['id']) && $_GET['id']!=''){
                    $conn = mysqli_connect("localhost", "root", "", "xpto");
                    if ($conn -> connect_error){
                    die("connection failed:".$conn-> connect_error);
                    }
                    $sql =  "SELECT * FROM routes WHERE route_id=".$_GET['id']."";
                    $result = $conn->query($sql);
                    $rows = $result->fetch_assoc();
                ?>
                    <form action="includes/routes-update.php" method="POST" class="route-forms">
                    <h3>Update Route</h3>
                    <input type="hidden" name="route_id" id="route_id" value="<?php echo $rows['route_id'];?>">
                    <label for="warehouse-name" class="col-form-label">Warehouse:</label>
                    <input type="text" name="warehouse" class="form-control" id="warehouse-name" value="<?php echo $rows['origin_route']; ?>">
                    <label for="route-name" class="col-form-label">Route Name:</label>
                    <input type="text" name="routename" class="form-control" id="route-name" value="<?php echo $rows['route_name']; ?>">
                    <label for="route-cost" class="col-form-label">Route Cost:</label>
                    <input type="text" name="routecost" class="form-control" id="route-cost" value="<?php echo $rows['route_cost']; ?>">
                    <label for="shipping-time" class="col-form-label">Shipping Time:</label>
                    <input type="datetime-local" name="shippingtime" class="form-control" id="shipping-time" value="<?php echo date_format(strtotime($rows['delivery_time']),'dd/mm/yyyy h:i a') ?>">    
                    
                    <label for="route-distance" class="col-form-label">Route Distance:</label>
                    <input type="text" name="routedistance" class="form-control" id="route-distance" value="<?php echo $rows['route_distance']; ?>">
                    
                    
                    <label for="message-text" class="col-form-label">Status:</label>
                    <input type="text" name="status" class="form-control" id="route-status" value="<?php echo $rows['route_status']; ?>"> 
                    <button class="btn mt-3" id="routeupdate-btncancel" type="button" name="cancel">Cancel</button>                   
                    <button class="btn mt-3" type="submit" name="save" >Save</button> 
                </form>
                <?php
                    }
                ?>
            </div>
        </div>
    </section>
    <script>
        $( document ).ready(function() {
            $("#routeupdate-btncancel").click(function(){
                $("#routes-update").hide();
            });
        });
    </script>
    <script>
        $("#routes-add").hide();
        $( document ).ready(function() {
            $("#route-btnadd").click(function(){
                $("#routes-add").show();
            });
        });
        $( document ).ready(function() {
            $("#routeadd-btncancel").click(function(){
                $("#routes-add").hide();
            });
        });
    </script>
</main>